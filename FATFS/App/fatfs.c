/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file   fatfs.c
  * @brief  Code for fatfs applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
#include "fatfs.h"

uint8_t retSD;    /* Return value for SD */
char SDPath[4];   /* SD logical drive path */
FATFS SDFatFS;    /* File system object for SD logical drive */
FIL SDFile;       /* File object for SD */

/* USER CODE BEGIN Variables */
uint8_t workBuffer[2 * _MAX_SS];
/* USER CODE END Variables */

void MX_FATFS_Init(void)
{
  /*## FatFS: Link the SD driver ###########################*/
  retSD = FATFS_LinkDriver(&SD_Driver, SDPath);

  /* USER CODE BEGIN Init */
  /* additional user code for init */
  /* USER CODE END Init */
}

/**
  * @brief  Gets Time from RTC
  * @param  None
  * @retval Time in DWORD
  */
DWORD get_fattime(void)
{
  /* USER CODE BEGIN get_fattime */
  return 0;
  /* USER CODE END get_fattime */
}

/* USER CODE BEGIN Application */
FRESULT formatSDCard(void) {
	return f_mkfs(SDPath, FM_ANY, 0, workBuffer, sizeof(workBuffer));
}

FRESULT mountSDCard(void) {
	return f_mount(&SDFatFS, (TCHAR const*) SDPath, 0);
}

FRESULT unMountSDCard(void) {
	return f_mount(NULL, (TCHAR const*) SDPath, 0);
}

FRESULT openSDCardFile(void) {
	//filenames are really short!!!
	return f_open(&SDFile, "FES.txt", FA_OPEN_APPEND | FA_WRITE);
}

FRESULT closeSDCardFile(void) {
	f_close(&SDFile);
	return f_sync(&SDFile);
}

FRESULT writeToSD(uint8_t *buffer, UINT length) {
	FRESULT res; /* FatFs function common result code */
	uint32_t byteswritten; /* File write/read counts */

	res = f_write(&SDFile, buffer, length, (void*) &byteswritten);

	if ((byteswritten > 0) && (res == FR_OK)) {
		return FR_OK;
	}
	return FR_INT_ERR;
}

/* USER CODE END Application */
